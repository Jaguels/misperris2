from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponseRedirect
from .models import Cliente
from django.views.generic import CreateView, ListView, FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse_lazy
from .forms import ClienteForm, FormularioLogin
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from Apps.Mascota.models import Mascota

from django.contrib.auth.models import User
# Create your views here.

def home (request):
    return render(request, 'index.html', {})

def inde (request):
    return render(request, 'login.html', {})

def salir(request):
    logout(request)
    return render(request, 'index.html', {})

def mascotas(request):
    return render(request, 'mascotas.html', {})

def listar (request):
    return render(request, 'listarPet.html', {})

def adoptar(request):
    return render(request, 'Confirmar.html', {})

# Create your views here.

class Login(FormView):
    #Establecemos la plantilla a utilizar
    template_name = 'login.html'
    #Le indicamos que el formulario a utilizar es el formulario de autenticación de Django
    form_class = FormularioLogin
    #Le decimos que cuando se haya completado exitosamente la operación nos redireccione a la url bienvenida de la aplicación personas
    success_url = ('mascotas')
 
    def dispatch(self, request, *args, **kwargs):
        #Si el usuario está autenticado entonces nos direcciona a la url establecida en success_url
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        #Sino lo está entonces nos muestra la plantilla del login simplemente
        else:
            return super(Login, self).dispatch(request, *args, **kwargs)
 
    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(Login, self).form_valid(form)

    def salir(self,request):
        salir(request)
        return redirect('login.html')

    def logOut(self,request):
        salir(request)
        return redirect('login.html')

 
class ClienteCreate(CreateView):
    model = User
    form_class = ClienteForm
    template_name = 'registro.html'
    success_url = reverse_lazy('home')



