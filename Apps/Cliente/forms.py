from django import forms
from .models import Cliente
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from Apps.Mascota.models import Mascota
from django.contrib.auth.models import User


#tipo_list = [
#   ('casa con patio grande','Casa con patio grande'),
#  ('casa con patio chico', 'Casa con patio chico'),
# ('casa sin patio','Casa sin patio'),
#('departamento', 'Departamento'),
#]
#'vivienda': forms.Select(choices= tipo_list) 
class ClienteForm(UserCreationForm):

    class Meta:
        model = User
        
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
            

        ]

        labels = {
            'username': 'Nombre de usuario',
            'first_name': 'Nombre',
            'last_name': 'Apellidos',
            'email': 'Email',
            'password1':'Ingrese Clave',
            'password2': 'Confirme Clave',
        }

        widgets = {
            'username': forms.TextInput(attrs={'class':'form-control'}),
    		'first_name': forms.TextInput(attrs={'class':'form-control'}),
            'last_name': forms.TextInput(attrs={'class':'form-control'}),
    		'email': forms.TextInput(attrs={'class':'form-control'}),
    		'password1': forms.PasswordInput(attrs={'class':'form-control'}),
            'password2': forms.PasswordInput(attrs={'class':'form-control'}),
            
        }

class FormularioLogin(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(FormularioLogin, self).__init__(*args, **kwargs)        
        self.fields['username'].widget.attrs['class'] = 'form-control' 
        self.fields['username'].widget.attrs['placeholder'] = 'Ingrese Nombre'       
        self.fields['password'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['placeholder'] = 'Ingrese Clave'


estado_list = [
    ('adoptado','ADOPTADO'),
    ('rescatado', 'RESCATADO'),
    ('disponible','DISPONIBLE'),
]

class MascotaForm(forms.ModelForm):
    class Meta:
        model = Mascota
        
        fields = [
            'nombre',
            'foto',
            'raza',
            'descripcion',
            'estado',
            

        ]

        labels = {
            'nombre': 'Nombre',
            'foto': 'Foto',
            'raza': 'Raza',
    		'descripcion': 'Descripcion', 
    		'estado': 'Estado', 
    		
        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
    		'foto': forms.FileInput,
            'raza': forms.TextInput(attrs={'class':'form-control'}),
    		'descripcion': forms.Textarea(attrs={'class':'form-control'}),
    		'estado': forms.Select(choices=estado_list), 
    		
        }

