from django.urls import path
from django.conf.urls import url, include
from . import views
from Apps.Cliente.views import ClienteCreate, Login, home, salir, mascotas
from django.contrib.auth import login
from django.views.generic import TemplateView
from Apps.Mascota.views import MascotaCreate, PetList,PetUpdate, PetDelete,PetListR , PetListAll,PetConfir, PetGallery,PetGalleryA,PetGalleryR
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^ingresar$', Login.as_view(), name='login'),
    url(r'^mascotas$', login_required(PetGallery.as_view()), name="listarD"),
    url(r'^listarA$', login_required(PetGalleryA.as_view()), name='listarAa'),
    url(r'^listarR$', login_required(PetGalleryR.as_view()), name='listarR'),
    url(r'^salir$', views.salir ,name='salir'),
    url(r'^$', views.home, name='home' ),
    url(r'^registrarM$', login_required(MascotaCreate.as_view()), name='registrarM'),
    url(r'^listarM$', login_required(PetList.as_view()), name='listarM'),
    url(r'^listar$', login_required(PetListR.as_view()), name='listarRr'),  
    url(r'^listarAll$', login_required(PetListAll.as_view()), name='listarAll'), 
    url(r'^ModificarM/(?P<pk>\d+)/$', login_required(PetUpdate.as_view()), name='ModificarM'),
    url(r'^EliminarM/(?P<pk>\d+)/$', login_required(PetDelete.as_view()), name='EliminarM'),
    url(r'^confirmar/(?P<pk>\d+)/$', login_required(PetConfir.as_view()), name='confirmar'),
    url(r'^salir/$', views.salir, name='logOut'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


    #url(r'^$', TemplateView.as_view(template_name = 'baseLogin.html'), name='home'),

    #path('', views.home, name='home'),


    path('registroCliente/', views.ClienteCreate.as_view(),
    name = 'registroCli' ),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
