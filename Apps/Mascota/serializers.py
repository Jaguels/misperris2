from rest_framework import serializers
from .models import Mascota

class MascotaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mascota
        fields =(
            'nombre',
            'foto',
            'raza',
            'descripcion',
            'estado',
        )
