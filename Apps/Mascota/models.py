from django.db import models

# Create your models here.

class Mascota(models.Model):
    nombre = models.CharField(max_length = 50)
    foto = models.ImageField(upload_to='upload/')
    raza = models.CharField(max_length = 50)
    descripcion = models.CharField(max_length = 255)
    estado = models.CharField( max_length=15)
    objects = models.Manager()