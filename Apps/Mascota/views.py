from django.shortcuts import render
from django.views.generic import UpdateView, CreateView, ListView, FormView, DeleteView
from Apps.Cliente.views import home, listar, mascotas
from Apps.Mascota.models import Mascota
from Apps.Cliente.forms import MascotaForm
from django.urls import reverse_lazy
import os


# Create your views here.




class PetListR(ListView):
    model= Mascota
    form_class=  MascotaForm   
    template_name= 'listarPet.html'

    def get_queryset(self):
            return Mascota.objects.filter(estado='rescatado')

class PetListAll(ListView):
    model= Mascota
    form_class=  MascotaForm   
    template_name= 'listarPet.html'

class MascotaCreate(CreateView):
    model = Mascota
    form_class = MascotaForm
    template_name = 'homePet.html'
    success_url = reverse_lazy('listarD')
    
class PetList(ListView):
    model= Mascota
    form_class=  MascotaForm   
    template_name= 'listarPet.html'

    def get_queryset(self):
            return Mascota.objects.filter(estado='disponible')

class PetUpdate(UpdateView):
    model= Mascota
    form_class = MascotaForm
    template_name= 'ModPet.html'
    success_url = reverse_lazy('listarM')

class PetDelete(DeleteView):
    model= Mascota
    template_name= 'eliminarPet.html'
    success_url = reverse_lazy('listarAll')

class PetGallery(ListView):
    model= Mascota
    form_class=  MascotaForm   
    template_name= 'mascotas.html'

    def get_queryset(self):
            return Mascota.objects.filter(estado='disponible')

class PetGalleryR(ListView):
    model= Mascota
    form_class=  MascotaForm   
    template_name= 'mascotas.html'

    def get_queryset(self):
            return Mascota.objects.filter(estado='rescatado')


class PetGalleryA(ListView):
    model= Mascota
    form_class=  MascotaForm   
    template_name= 'mascotas.html'

    def get_queryset(self):
            return Mascota.objects.filter(estado='adoptado')

class PetConfir(DeleteView):
    model= Mascota
    template_name= 'Confirmar.html'
    success_url = reverse_lazy('listarD')

