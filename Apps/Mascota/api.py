from .serializers import MascotaSerializer
from .models import Mascota
from rest_framework import generics
from django.shortcuts import get_object_or_404


class PerroList(generics.ListCreateAPIView):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializer

    def perform_create(self, serializer):
        serializer.save()

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class PerroDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializer
