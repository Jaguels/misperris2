from django.urls import path
from django.conf.urls import url, include
from . import views
from Apps.Mascota.views import MascotaCreate
from django.contrib.auth import login
from django.views.generic import TemplateView
from .api import PerroList, PerroDetail

urlpatterns = [
    url(r'^ApiMascota/$', PerroList.as_view(), name='MascotaCreate'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

]
